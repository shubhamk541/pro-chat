import React from "react";
import { Segment, Button, Input } from "semantic-ui-react";
import uuidv4 from "uuid/v4";
import firebase from "../../firebase";
import FileModal from "./FileModal";
import ProgressBar from "./ProgressBar";

class MessageForm extends React.Component {
  state = {
    storageRef: firebase.storage().ref(),
    uploadState: "",
    uploadTask: null,
    message: "",
    loading: false,
    channel: this.props.channel,
    user: this.props.currentUser,
    errors: [],
    modal: false,
    percentageUpload: 0
  };

  openModal = () => {
    this.setState({ modal: true });
  };

  closeModal = () => {
    this.setState({ modal: false });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  createMessage = (fileUrl = null) => {
    const message = {
      timestamp: firebase.database.ServerValue.TIMESTAMP,
      user: {
        id: this.state.user.uid,
        avatar: this.state.user.photoURL,
        name: this.state.user.displayName
      }
    };
    if (fileUrl !== null) {
      message["image"] = fileUrl;
    } else {
      message["content"] = this.state.message;
    }
    return message;
  };

  //UPLOAD FILE CODE

 getPath=()=>{
    if(this.props.isPrivateChannel){
      return `chat/private-${this.state.channel.id}`;
    }
    else
      return `chat/public`
 }

  uploadFile = (file, type) => {
    console.log(type);
    const pathToUpload = this.state.channel.id;
    const ref = this.props.getMessagesRef();
    const filePath = `${this.getPath()}/${uuidv4()}.jpg`;
    //console.log(filePath)
    this.setState(
      {
        uploadState: "uploading",
        uploadTask: this.state.storageRef.child(filePath).put(file, type)
      },
      () => {
        this.state.uploadTask.on(
          "state_changed",
          snap => {
            const percentageUpload = Math.round(
              (snap.bytesTransferred / snap.totalBytes) * 100
            );
            this.props.isProgressBarVisible(percentageUpload);
            this.setState({ percentageUpload });
          },
          err => {
            console.log(err);
            this.setState({
              errors: this.state.errors.concat(err),
              uploadTask: null,
              uploadState: "error"
            });
          },
          () => {
            this.state.uploadTask.snapshot.ref
              .getDownloadURL()
              .then(downloadUrl => {
                this.sendFileMessage(downloadUrl, ref, pathToUpload);
              })
              .catch(err => {
                console.log(err);
                this.setState({
                  errors: this.state.errors.concat(err),
                  uploadTask: null,
                  uploadState: "error"
                });
              });
          }
        );
      }
    );
  };

  sendFileMessage = (fileUrl, ref, pathToUpload) => {
    ref
      .child(pathToUpload)
      .push()
      .set(this.createMessage(fileUrl))
      .then(() => {
        this.setState({
          uploadState: "done"
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          errors: this.state.errors.concat(err)
        });
      });
  };

  showInputErrorClass = (errors, input) => {
    return errors.some(error => error.message.toLowerCase().includes(input))
      ? "error"
      : "";
  };

  sendMessage = () => {
    const { getMessagesRef } = this.props;
    const { message, channel } = this.state;
    if (message.length > 0) {
     // console.log(channel);
      if (channel === null) alert("Channels Not Available");
      else {
        this.setState({ loading: true });
        getMessagesRef()
          .child(channel.id)
          .push()
          .set(this.createMessage())
          .then(() => {
            console.log("Uploaded");
            this.setState({ loading: false, message: "", errors: [] });
          })
          .catch(err => {
            this.setState({
              loading: false,
              errors: this.state.errors.concat(err)
            });
          });
      }
    } else {
      console.log("Error");
      this.setState({
        errors: this.state.errors.concat({ message: "Please Add a Message" })
      });
    }
  };

  render() {
    const { errors, message, loading, modal,percentageUpload,uploadState } = this.state;
    return (
      <Segment className="message__form">
        <Input
          fluid
          name="message"
          style={{ marginBottom: "0.7 em" }}
          label={<Button icon="add" />}
          value={message}
          labelPosition="left"
          placeholder="Write Your Messages"
          className={this.showInputErrorClass(errors, "message")}
          onChange={this.handleChange}
        />
        <Button.Group icon widths="2">
          <Button
            disabled={loading}
            color="orange"
            content="Add Reply"
            labelPosition="left"
            icon="edit"
            onClick={this.sendMessage}
          />
          <Button
            disabled={uploadState==='uploading'}
            color="olive"
            content="Add Media"
            labelPosition="right"
            icon="cloud upload"
            onClick={this.openModal}
          />
       </Button.Group>
          <FileModal
            modal={modal}
            closeModal={this.closeModal}
            uploadFile={this.uploadFile}
          />
        <ProgressBar
        uploadState={uploadState}
        percentageUpload={percentageUpload}/>
      </Segment>
    );
  }
}
export default MessageForm;
