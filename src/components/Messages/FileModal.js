import React from "react";
import mime from 'mime-types';

import { Modal, Input, Button, Icon } from "semantic-ui-react";

class FileModal extends React.Component {
  state = {
    file: null,
    filetype:['image/png','image/jpeg','application/pdf']
  };
  addFile = event => {
    const file = event.target.files[0];
    if (file) {
      this.setState({ file: file });
    }
  };
  sendFile=()=>{
      const {file}=this.state;
      const {uploadFile,closeModal}=this.props;
      if(file!==null){
          console.log(file.type);
          if(this.isAuthorised(file.type)){
              //console.log("Correct");
             const metaData={contentType:mime.lookup(file.name)}
             uploadFile(file,metaData);
             this.clearFile();
             closeModal();
          }
      }
  }
  clearFile=()=>{
      this.setState({file:null});
  }
  isAuthorised=(filetype)=>this.state.filetype.includes(filetype)
  render() {
    const { modal, closeModal } = this.props;
    return (
      <Modal size="small" open={modal} onClose={closeModal}>
        <Modal.Header>Select File</Modal.Header>
        <Modal.Content>
          <Input
            onChange={this.addFile}
            fluid
            label="File Type: PDF/JPEG/MP4/PNG"
            type="file"
            name="file"
          />
        </Modal.Content>
        <Modal.Actions>
          <Button color="green" onClick={this.sendFile}>
            <Icon name="checkmark" />
            Send
          </Button>
          <Button color="red" onClick={closeModal}>
            <Icon name="close" />
            Cancel
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}
export default FileModal;
