import React from "react";
import { Segment, Header, Icon,Input } from "semantic-ui-react";

class MessagesHeader extends React.Component {
  render() {
    const {handleSearchChange,channelName,numUniqueUsers,searchLoading,isPrivateChannel,isChannelStarred}=this.props;
    return (
      <Segment clearing>
        {/* Channel Title*/}

        <Header fluid="true" as="h2" floated="left" style={{ marginBottom: 0 }}>
          <span>
            {channelName}
           {!isPrivateChannel&&<Icon onClick={this.props.handleStar} name={isChannelStarred?'star':"star outline"} color={isChannelStarred?'yellow':"black" }/>}
          </span>
         {!isPrivateChannel&& <Header.Subheader>{numUniqueUsers}</Header.Subheader>}
        </Header>

        {/* Channel Search Header*/}
        <Header floated="right">
        <Input
        loading={searchLoading}
        icon='search'
        onChange={handleSearchChange}
        size='mini'
        name='searchTerm'
        placeholder='Search Messages'
        />         
        </Header>
      </Segment>
    );
  }
}
export default MessagesHeader;