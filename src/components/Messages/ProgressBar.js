import React from 'react';
import {Progress} from 'semantic-ui-react';

const ProgressBar=({uploadState,percentageUpload})=>(
    uploadState ==='uploading' &&(
        <Progress
         className="progress__bar"
         percent={percentageUpload}
         progress
         indicating
         size='medium'
        />
    )
)
export default ProgressBar;