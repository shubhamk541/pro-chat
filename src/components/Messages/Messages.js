import React from "react";
import { Segment, Comment } from "semantic-ui-react";
import MessageForm from "./MessageForm";
import MessagesHeader from "./MessagesHeader";
import firebase from "../../firebase";
import Message from "./Message";

class Messages extends React.Component {
  state = {
    isPrivateChannel: this.props.isPrivateChannel,
    privateMessagesRef: firebase.database().ref("privateMessages"),
    messagesRef: firebase.database().ref("messages"),
    currentChannel: this.props.currentChannel,
    user: this.props.currentUser,
    usersRef: firebase.database().ref("users"),
    messages: [],
    messagesLoading: true,
    progressBar: false,
    numUniqueUsers: "",
    searchTerm: "",
    searchLoading: false,
    searchResults: [],
    isChannelStarred: ""
  };

  componentDidMount() {
    const { currentChannel, user } = this.state;
    if (currentChannel && user) {
      this.addListener(currentChannel.id);
      this.addStarUserListener(currentChannel.id, user.uid);
    }
  }

  addListener = channelId => {
    this.addMessageListener(channelId);
  };

  addStarUserListener = (channelId, userId) => {
    this.state.usersRef
      .child(userId)
      .child("starred")
      .once("value")
      .then(data => {
        if (data.val() !== null) {
          const channelIds = Object.keys(data.val());
          const prevstarred = channelIds.includes(channelId);
          this.setState({ isChannelStarred: prevstarred });
        }
      });
  };

  addMessageListener = channelId => {
    let loadedMessages = [];
    const ref = this.getMessagesRef();
    ref.child(channelId).on("child_added", snap => {
      loadedMessages.push(snap.val());
      this.setState({ messages: loadedMessages, messagesLoading: false });
      this.countuniqueUser(loadedMessages);
    });
  };

  getMessagesRef = () => {
    const { isPrivateChannel, messagesRef, privateMessagesRef } = this.state;
    return isPrivateChannel ? privateMessagesRef : messagesRef;
  };

  handleSearchChange = event => {
    this.setState(
      {
        searchTerm: event.target.value,
        searchLoading: true
      },
      () => this.handleSearchMessages()
    );
  };

  handleSearchMessages = () => {
    const channelMessages = [...this.state.messages];
    const regex = new RegExp(this.state.searchTerm, "gi");
    const searchResults = channelMessages.reduce((acc, message) => {
      if (
        (message.content && message.content.match(regex)) ||
        message.user.name.match(regex)
      )
        acc.push(message);
      return acc;
    }, []);
    this.setState({ searchResults });
    setTimeout(() => this.setState({ searchLoading: false }), 1000);
  };

  countuniqueUser = messages => {
    const uniqueusers = messages.reduce((acc, message) => {
      if (!acc.includes(message.user.name)) {
        acc.push(message.user.name);
      }
      return acc;
    }, []);
    const plural = uniqueusers.length > 1 || uniqueusers.length === 0;
    const numUniqueUsers = `${uniqueusers.length} user${plural ? "s" : ""}`;
    this.setState({ numUniqueUsers });
  };

  displayMessages = messages =>
    messages.length > 0 &&
    messages.map(message => (
      <Message
        user={this.state.user}
        key={message.timestamp}
        message={message}
      />
    ));

  isProgressBarVisible = percent => {
    if (percent > 0) this.setState({ progressBar: true });
  };

  channelName = channel =>
    channel ? `${this.state.isPrivateChannel ? "@" : "#"}${channel.name}` : "";

  handleStar = () => {
    this.setState(
      prevState => ({
        isChannelStarred: !prevState.isChannelStarred
      }),
      () => this.starChannel()
    );
  };

  starChannel = () => {
    if (this.state.isChannelStarred) {
      this.state.usersRef.child(`${this.state.user.uid}/starred`).update({
        [this.state.currentChannel.id]: {
          name: this.state.currentChannel.name,
          details: this.state.currentChannel.details,
          createdBy: {
            name: this.state.currentChannel.createdBy.name,
            avatar: this.state.currentChannel.createdBy.avatar
          }
        }
      });
    } else {
      this.state.usersRef
        .child(`${this.state.user.uid}/starred`)
        .child(this.state.currentChannel.id)
        .remove(err => {
          if (err !== null) console.log(err);
        });
    }
  };
  render() {
    const {
      messagesRef,
      currentChannel,
      user,
      messages,
      progressBar,
      numUniqueUsers,
      searchResults,
      searchTerm,
      searchLoading,
      isPrivateChannel,
      isChannelStarred
    } = this.state;
    return (
      <React.Fragment>
        <MessagesHeader
          isPrivateChannel={isPrivateChannel}
          channelName={this.channelName(currentChannel)}
          numUniqueUsers={numUniqueUsers}
          handleSearchChange={this.handleSearchChange}
          searchLoading={searchLoading}
          handleStar={this.handleStar}
          isChannelStarred={isChannelStarred}
        />
        <Segment>
          <Comment.Group
            className={progressBar ? "messages__progress" : "messages"}
          >
            {/*Message*/}

            {searchTerm
              ? this.displayMessages(searchResults)
              : this.displayMessages(messages)}
          </Comment.Group>
        </Segment>
        <MessageForm
          messagesRef={messagesRef}
          channel={currentChannel}
          currentUser={user}
          isProgressBarVisible={this.isProgressBarVisible}
          isPrivateChannel={isPrivateChannel}
          getMessagesRef={this.getMessagesRef}
        />
      </React.Fragment>
    );
  }
}
export default Messages;
