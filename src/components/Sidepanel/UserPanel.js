import React from "react";
import { Grid, Header, Icon, Dropdown, Image } from "semantic-ui-react";
import firebase from "../../firebase";

class UserPanel extends React.Component {
  state = {
    user: this.props.currentUser
  };
  dropDownOptions = () => [
    {
      key: "user",
      text: (
        <span>
          Login as <strong>{this.state.user.displayName}</strong>
        </span>
      ),
      disabled: true
    },
    {
      key: "avatar",
      text: <span>Change Avatar</span>
    },
    {
      key: "logout",
      text: <span onClick={this.onSignOut}>Sign Out</span>
    }
  ];
  onSignOut = () => {
    firebase
      .auth()
      .signOut()
      .then(() => console.log("signOut"));
  };
  render() {
    const { user } = this.state;

    return (
      <Grid style={{ background: "#087bd3" }}>
        <Grid.Column>
          <Grid.Row style={{ padding: "1.2em", margin: 0 }}>
            <Header inverted floated="left" as="h2">
              <Icon name="user" />
              <Header.Content>ProChat</Header.Content>
            </Header>
          </Grid.Row>

          {/*Grid Dropdown*/}
          <Header style={{ padding: "0.25em" }} as="h3" inverted>
            <Dropdown
              trigger={
                <span>
                  <Image src={user.photoURL} spaced="right" avatar />
                   {user.displayName}
                </span>
              }
              options={this.dropDownOptions()}
            />
          </Header>
        </Grid.Column>
      </Grid>
    );
  }
}
export default UserPanel;
