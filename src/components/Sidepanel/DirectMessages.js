import React from "react";
import { Menu, Icon } from "semantic-ui-react";
import firebase from "../../firebase";

import {connect} from 'react-redux';
import {setChannel,setPrivateChannel} from '../../actions/index';


class DirectMessages extends React.Component {
  state = {
    activeChannel:'',
    user: this.props.currentUser,
    users: [],
    userRef: firebase.database().ref("users"),
    connectedRef: firebase.database().ref(".info/connected"),
    presenceRef: firebase.database().ref("presence")
  };
  componentDidMount() {
    if (this.state.user) {
      this.addListeners(this.state.user.uid);
    }
  }
  addListeners = currentUserUid => {
    let loadedUsers = [];
    this.state.userRef.on("child_added", snap => {
      if (currentUserUid !== snap.key) {
        let user = snap.val();
        user["uid"] = snap.key;
        user["status"] = "offline";
        loadedUsers.push(user);
        this.setState({ users: loadedUsers });
      }
    });
    this.state.connectedRef.on("value", snap => {
      if (snap.val() === true) {
        const ref = this.state.presenceRef.child(currentUserUid);
        ref.set(true);
        ref.onDisconnect().remove(err => {
          if (err !== null) console.log(err);
        });
      }
    });
    //Listen on new Added User authenticated
    this.state.presenceRef.on('child_added',snap=>{
        if(currentUserUid !== snap.key)
        {
            this.addStatusToUser(snap.key);
        }
    })
    //Listen on User removed i.e offline
    this.state.presenceRef.on('child_removed',snap=>{
        if(currentUserUid!== snap.key)
        {
            this.addStatusToUser(snap.key,false);
        }
    })
  };

  addStatusToUser=(userId,connected = true)=>{
      const updatedUsers=this.state.users.reduce((acc,user)=>{
             if(user.uid===userId){
                 user['status']=`${connected?'online':'offline'}`
             }
             return acc.concat(user);
      },[])
      this.setState({users:updatedUsers})
  }

  changeChannel=user=>{
      const channelId=this.getChannelId(user.uid);
      const channelData={
          id:channelId,
          name:user.name
      }
      this.props.setChannel(channelData);
      this.props.setPrivateChannel(true);
      this.setActiveChannel(user.uid);
  }

  setActiveChannel=uId=>{
      this.setState({activeChannel:uId});
  }
  getChannelId=userId=>{
        const currentUserUid=this.state.user.uid;
        return userId < currentUserUid ? `${userId}/${currentUserUid}`:`${currentUserUid}/${userId}`;
  }

  isUserOnline=user=> user.status === 'online'
  render() {
    const { users,activeChannel } = this.state;
    return (
      <Menu.Menu className="menu">
        <Menu.Item>
          <span>
            <Icon name="mail" />
            Direct Messages
          </span>{" "}
          ({users.length})
        </Menu.Item>
        {users.map(user=>(
            <Menu.Item
            active={activeChannel===user.uid}
            key={user.uid}
            onClick={()=>this.changeChannel(user)}
            style={{opacity:0.7,fontStyle:'italic'}}
            >
            <Icon
            name='circle'
            color={this.isUserOnline(user)?'green':'red'}
            ></Icon>
            @{user.name}
            </Menu.Item>
        ))}
      </Menu.Menu>
    );
  }
}
export default connect(null,{setChannel,setPrivateChannel})(DirectMessages);
