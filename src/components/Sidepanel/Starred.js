import React from 'react';
import {Menu,Icon} from 'semantic-ui-react';
import {setChannel,setPrivateChannel} from '../../actions/index';
import {connect} from 'react-redux';
import firebase from '../../firebase';


class Starred extends React.Component{

  state={
       starredChannels:[],
       activeChannel:'',
       user:this.props.currentUser,
       usersRef:firebase.database().ref('users'),
  }

  componentDidMount(){
    if(this.state.user){
      this.addListener(this.state.user.uid);
    }
  }

  addListener=(userId)=>{
    this.state.usersRef
    .child(userId)
    .child('starred')
    .on('child_added',snap=>{
      const starredChannels={id:snap.key,...snap.val()};
      console.log(starredChannels);
      this.setState({
        starredChannels: [...this.state.starredChannels,starredChannels]
      })
    })
    this.state.usersRef
    .child(userId)
    .child('starred')
    .on('child_removed',snap=>{
      const channelToRemove={id:snap.key,...snap.val()}
      const filteredChannels=this.state.starredChannels.filter(channel=>{
        return channel.id!==channelToRemove.id;
      })
      this.setState({starredChannels:filteredChannels})
    })
  }

  setActiveChannel =(channel)=>
  {
      this.setState({activeChannel:channel.id});
  }

  changeChannel = channel=>
  {
      this.setActiveChannel(channel);
      this.props.setChannel(channel);
      this.props.setPrivateChannel(false);
      this.setState({channel});
  }

  displayChannels = starredChannels =>
    starredChannels.length > 0 &&
    starredChannels.map(channel => (
      <Menu.Item
        key={channel.id}
        name={channel.name}
        onClick={() => {this.changeChannel(channel)}}
        style={{ opacity: 0.8 }}
        active={channel.id===this.state.activeChannel}
      >
        # {channel.name}
      </Menu.Item>
    ));

    render(){
      const {starredChannels}=this.state;
        return(
            <Menu.Menu className="menu">
          <Menu.Item>
            <span>
              <Icon name="star" /> STARRED
            </span>{" "}
            [{starredChannels.length}] 
          </Menu.Item>
          {/* Channels List */}
          {this.displayChannels(starredChannels)}
        </Menu.Menu>
        )
    }

}
export default connect(null,{setPrivateChannel,setChannel})(Starred);