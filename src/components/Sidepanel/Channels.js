import React from "react";
import { Menu, Icon, Modal, Input, Form, Button, Label } from "semantic-ui-react";
import firebase from "../../firebase";
import {connect} from 'react-redux';
import {setChannel,setPrivateChannel} from '../../actions/index';

class Channels extends React.Component {
  state = {
    channel:null,
    messagesRef:firebase.database().ref('messages'),
    notifications:[],
    channels: [],
    activeChannel:'',
    channelName: "",
    channelDescription: "",
    channelsRef: firebase.database().ref("channels"),
    modal: false,
    user: this.props.currentUser,
    firstLoad:true
  };

  componentDidMount = () => {
    this.loadChannel();
  };

  loadChannel = () => {
    let loadedChannel = [];
    this.state.channelsRef.on("child_added", snap => {
      loadedChannel.push(snap.val());
      console.log("channel Loaded");
      this.setState({ channels: loadedChannel},() => this.setFirstChannel());
      this.addNotificationListener(snap.key);
    });
  };
  //  notifications to channels
   addNotificationListener=channelId=>{
     this.state.messagesRef.child(channelId).on('value',snap=>{
       if(this.state.channel){
         this.handleNotifications(channelId,this.state.channel.id,this.state.notifications,snap);
       }
     })
   }

  handleNotifications=(channelId,currentChannelId,notifications,snap)=>{
    let lastTotal=0;
    const index=notifications.findIndex(notification=>notification.id===channelId)
    if(index!==-1){
      if(channelId!==currentChannelId){
        lastTotal=notifications[index].total;
      if(snap.numChildren()-lastTotal>0){
        notifications[index].count=snap.numChildren()-lastTotal;
      }}
      notifications[index].lastKnownTotal=snap.numChildren();
    }
    else{
       notifications.push({
         id:channelId,
         total:snap.numChildren(),
         lastKnownTotal:snap.numChildren(),
         count:0
       })
    }
   this.setState({notifications});
  }

 // Stop load Channel When go to different routes
  componentWillUnmount=()=>{
      this.stoploadChannel();
  }
  stoploadChannel=()=>{
    this.state.channelsRef.off();
  }

  setFirstChannel=()=>{
      const firstChannel=this.state.channels[0];
      if(this.state.firstLoad&&this.state.channels.length>0){
          this.setActiveChannel(firstChannel);
          this.props.setChannel(firstChannel);
          this.setState({channel:firstChannel});
      }
      this.setState({firstLoad:false});
  }
//   Put Channels Globally in Redux
   changeChannel = channel=>
   {
       this.setActiveChannel(channel);
       this.clearNotifications();
       this.props.setChannel(channel);
       this.props.setPrivateChannel(false);
       this.setState({channel});
   }

   clearNotifications=()=>{
     let index=this.state.notifications.findIndex(notification=>notification.id===this.state.channel.id)
     if(index!==-1){
       let updatedNotifications=[...this.state.notifications];
       updatedNotifications[index].total=this.state.notifications[index].lastKnownTotal;
       updatedNotifications[index].count=0;
       this.setState({notifications:updatedNotifications})

     }
   }

   setActiveChannel =(channel)=>
   {
       this.setState({activeChannel:channel.id});
   }

  getNotificationsCount=channel=>{
    let count=0;
    this.state.notifications.forEach(notification=>{
      if(notification.id === channel.id)
      {
        count=notification.count;
      }
    })
    console.log(count);
    if(count>0) return count;
  }

  displayChannels = channels =>
    channels.length > 0 &&
    channels.map(channel => (
      <Menu.Item
        key={channel.id}
        name={channel.name}
        onClick={() => {this.changeChannel(channel)}}
        style={{ opacity: 0.8 }}
        active={channel.id===this.state.activeChannel}
      >
       {this.getNotificationsCount(channel)&&(
         <Label color='black'>{this.getNotificationsCount(channel)}</Label>
       )}
        # {channel.name}
      </Menu.Item>
    ));

  addChannel = ({ channelsRef, channelName, channelDescription, user }) => {
    const key = channelsRef.push().key;
    const newChannel = {
      id: key,
      name: channelName,
      details: channelDescription,
      createdBy: {
        name: user.displayName,
        avatar: user.photoURL
      }
    };

    channelsRef
      .child(key)
      .update(newChannel)
      .then(() => {
        this.setState({ channelName: "", channelDescription: "" });
        this.closeModal();
        console.log("Channel Added");
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (this.isFormValid(this.state)) {
      console.log("Channel added");
      this.addChannel(this.state);
    }
  };

  isFormValid = ({ channelName, channelDescription }) =>
    channelName && channelDescription;

  openModal = () => this.setState({ modal: true });
  closeModal = () => this.setState({ modal: false });

  onHandleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { channels, modal } = this.state;
    return (
      <React.Fragment>
        <Menu.Menu className="menu">
          <Menu.Item>
            <span>
              <Icon name="exchange" /> CHANNELS
            </span>{" "}
            [{channels.length}] <Icon onClick={this.openModal} name="add" />
          </Menu.Item>
          {/* Channels List */}
          {this.displayChannels(channels)}
        </Menu.Menu>
        {/*Modal For channels*/}
        <Modal
          dimmer="blurring"
          size="small"
          open={modal}
          onClose={this.closeModal}
        >
          <Modal.Header>Add Channel</Modal.Header>
          <Modal.Content>
            <Form onSubmit={this.handleSubmit}>
              <Form.Field>
                <Input
                  fluid
                  label="Name Of Channel"
                  name="channelName"
                  onChange={this.onHandleChange}
                />
              </Form.Field>
              <Form.Field>
                <Input
                  fluid
                  label="Channel Description"
                  name="channelDescription"
                  onChange={this.onHandleChange}
                />
              </Form.Field>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={this.handleSubmit} color="green" inverted>
              <Icon name="check" /> Add
            </Button>
            <Button onClick={this.closeModal} color="red" inverted>
              <Icon name="remove" /> Cancel
            </Button>
          </Modal.Actions>
        </Modal>
      </React.Fragment>
    );
  }
}
export default connect(null,{setChannel,setPrivateChannel})(Channels);
