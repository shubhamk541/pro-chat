import React from "react";
import { Loader, Dimmer } from "semantic-ui-react";

const Spinner = () => {
  return (
    <Dimmer active>
      <Loader size="massive" content={"Chat is Loading....."} />
    </Dimmer>
  );
};
export default Spinner;
